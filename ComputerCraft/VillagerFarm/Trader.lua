trade = peripheral.wrap("right") -- The side of the trading post
pages = {}
saplings = {}
stations = {}
 
function getId() -- Retuns the villager ids needed for trading
  return trade.getVillagerIds()
end
 
function getTradeInfo(id) -- Returns trading information needed
  local sold = trade.getItemSold(id,0)
  local bought = trade.getItemBought(id,0)
  return sold.name, sold.qty, sold.rawName, bought.name, bought.qty, id
end
 
function isPageMissing(name) -- Checks if i already have the page or not
  for index, page in pairs(pages) do
    if page == name then
      return false
    end
  end
  return true
end

function isSapMissing(name) -- Checks if i already have the page or not
  for index, sap in pairs(saplings) do
    if sap == name then
      return false
    end
  end
  return true
end

function isStationMissing(name) -- Checks if i already have the page or not
  for index, station in pairs(stations) do
    if station == name then
      return false
    end
  end
  return true
end
 
function savePages() -- Saves the pages table in a file so that it will still remember all the pages after a reload
  if fs.exists("pages.dat") then
    fs.delete("pages.dat")
  end
  file = fs.open("pages.dat", "w")
  file.write(textutils.serialize(pages))
  file.close()
end

function saveSaplings() -- Saves the pages table in a file so that it will still remember all the pages after a reload
  if fs.exists("saplings.dat") then
    fs.delete("saplings.dat")
  end
  file = fs.open("saplings.dat", "w")
  file.write(textutils.serialize(saplings))
  file.close()
end

function saveStations() -- Saves the pages table in a file so that it will still remember all the pages after a reload
  if fs.exists("stations.dat") then
    fs.delete("stations.dat")
  end
  file = fs.open("stations.dat", "w")
  file.write(textutils.serialize(stations))
  file.close()
end
 
function loadPages() -- Load the pages table from save
  if fs.exists("pages.dat") then
    file = fs.open("pages.dat", "r")
    pages = textutils.unserialize(file.readAll())
    file.close()
  end
end

function loadSaplings() -- Load the pages table from save
  if fs.exists("saplings.dat") then
    file = fs.open("saplings.dat", "r")
    sapplings = textutils.unserialize(file.readAll())
    file.close()
  end
end

function loadStations() -- Load the pages table from save
  if fs.exists("stations.dat") then
    file = fs.open("stations.dat", "r")
    stations = textutils.unserialize(file.readAll())
    file.close()
  end
end

function buy(raw, item, id)
  if (raw == "item.myst.page" and (isPageMissing(item) or item == "Link Panel")) then --buy missing mystcraft pages
    if trade.performTrade(id, 0) then -- Then buy it
      print("Buying "..item)
      pages[#pages+1] = item -- Add the page to the table
      savePages() -- Save the pages table to file
    else
      print("Trade failed")
    end
  elseif (raw == "item.itemresource" or raw == "item.itemessence" or raw == "item.itemwispessence" or raw == "item.itemmanabean") then --buy thaumcraft items
    if trade.performTrade(id, 0) then -- Then buy it
      print("Buying "..item)
    else
      print("Trade failed")
    end
  elseif (raw == "item.openblocks.tuned_crystal" and isStationMissing(item)) then --buy openblocks radio crystals
    if trade.performTrade(id, 0) then -- Then buy it
      print("Buying "..item)
      stations[#stations+1] = item -- Add the sapling to the table
      saveStations() -- Save the saplings table to file
    else
      print("Trade failed")
    end
  elseif (raw == "item.sapling" and isSapMissing(item)) then --buy forestry sapplings
    if trade.performTrade(id, 0) then -- Then buy it
      print("Buying "..item)
      saplings[#saplings+1] = item -- Add the sapling to the table
      saveSaplings() -- Save the saplings table to file
    else
      print("Trade failed")
    end
  elseif (raw == "item.knowledgeFragment" or item =="Knowledge Fragment") then --buy forestry sapplings
    if trade.performTrade(id, 0) then -- Then buy it
      print("Buying "..item)
    else
      print("Trade failed")
    end
  end
end

loadPages()
loadSaplings()
loadStations()
term.clear()
term.setCursorPos(1,1)
while true do
  ids = getId() -- First get the ids of all close villagers
  for index, idnum in pairs(ids) do -- Loop through the ids
    sold,soldQty ,rawName,bought,boughtQty, id = getTradeInfo(idnum) -- Get information on what the villagers is trading
    buy(rawName, sold, id)
  end
  sleep(1)
end