local args = ...
local prog
local startup = "shell.run(\"/bb/Setup/setup\")"

if args == "LumberJack" then
  prog = "/bb/LumberYard/LumberJack"
elseif args == "Planter" then
  prog = "/bb/LumberYard/Planter"
elseif args == "Refiller" then
  prog = "/bb/LumberYard/Refiller"
elseif args == "FrontDoor" then
  prog = "/bb/Misc/FrontDoor"
elseif args == "CellMonitor" then
  prog = "/bb/PowerPlant/CellMonitor"
elseif args == "Trader" then
  prog = "/bb/VillagerFarm/Trader"
else
  error("That job does not exist")
end

startup = startup.."\n\nshell.run(\""..prog.."\")"


local f = fs.open("startup", "w")
f.write(startup)
f.close()

