local directories = {}
local files = {}
local baseURL = "https://bitbucket.org/kpfecteau/computercraft/raw/master/ComputerCraft/"

function addFile(dir, file)
  directories[#directories+1] = "/bb/"..dir
  for i=1, #file do
    files[#files+1] = dir.."/"..file[i]
  end
end

function createDirs()
  for i=1, #directories do
    local dir = directories[i]
    if not fs.exists(dir) then
      fs.makeDir(dir)
    end
  end
end

function dowloadFiles()
  createDirs()
  for i=1, #files do
    local content = http.get("https://bitbucket.org/kpfecteau/computercraft/raw/master/ComputerCraft/"..files[i]..".lua").readAll()
    if not content then
      error("Could not download file...")
    end
    local f = fs.open("/bb/"..files[i], "w")
    f.write(content)
    f.close()
    print("Downloaded "..files[i])
  end
end


addFile("LumberYard", {"LumberJack", "Planter", "Refiller"})
addFile("Setup", {"Job"})
addFile("Misc", {"FrontDoor"})
addFile("PowerPlant", {"CellMonitor"})
addFile("VillagerFarm", {"Trader"})
dowloadFiles()