local cell = peripheral.wrap("back")
local maxEnergy = cell.getMaxEnergyStored("back")
local currentEnergy

function setEngines(arg)
  if arg == "on" then
    print("Turning on engines...")
    rs.setBundledOutput("bottom", 0)
  elseif arg == "off" then
    print("Turning off engines...")
    rs.setBundledOutput("bottom", colors.white)
  end
end

function low()
  currentEnergy = cell.getEnergyStored("back")
  if currentEnergy < (maxEnergy*.05) then
    return true
  else
    return false
  end
end

function full()
  currentEnergy = cell.getEnergyStored("back")
  if currentEnergy > (maxEnergy - (maxEnergy*.05)) then
    return true
  else
    return false
  end
end




while true do
  currentEnergy = cell.getEnergyStored("back")
  print("Checking power...")
  if low() then
    print("Power is below 5%")
    setEngines("on")
    while not full() do
      sleep(10)
    end
  elseif full() then
    print("Power is above 95%")
    setEngines("off")
    while not low() do
      sleep(10)
    end
  end
end