print("Starting Planter program...")
print("Place saplings in slot 1")
print("Place corresponding log in slot 1")

while turtle.getItemCount(1) == 0 or turtle.getItemCount(16) == 0 do sleep(1) end

local ljID = 864
local rfID = 826
local label = os.getComputerLabel()
local xPos, yPos, zPos, face
 
function consolidate()
  for i=1, 15 do
    local notEmpty = false
    for j=i, 15 do
      if turtle.getItemCount(j) > 0 then
        notEmpty = true
        turtle.select(j)
        turtle.transferTo(i)
      end
    end
    if not notEmpty then
      break
    end
  end
end 
 
function plantSappling()
  consolidate()
  turtle.select(1)
  print("planting sappling...")
  turtle.place()
  
  while turtle.getItemCount(1) <= 5 do
    rednet.open("right")
    print("asking for refill...")
    rednet.send(rfID, "refill "..xPos.." "..zPos)
    print("message sent...")
    rednet.close("right")
    sleep(10)
  end
  turtle.select(16)
  print("done...")
end

function pickUpSapplings()
  turtle.suck()
  if not turtle.detectUp() then
    turtle.suckUp()
  end
  
  consolidate()
  
  for i=2, 15 do
    turtle.select(i)
    turtle.dropDown()
  end
  
  turtle.select(16)
end


function setLocationData()
  xPos, yPos, zPos = gps.locate()
  while turtle.getFuelLevel() < 6 do
    turtle.select(1)
    turtle.refuel(1)
    turtle.select(16)
  end 
  turtle.down()
  turtle.down()
  turtle.forward()
  
  local x, y, z = gps.locate()
  
  if z > zPos then
    face = 0
  elseif z < zPos then
    face = 2
  elseif x > xPos then
    face = 3
  elseif x < xPos then
    face = 1
  end
  
  turtle.back()
  turtle.up()
  turtle.up()
  
  if face == 0 then
    print("I am facing "..getCardinality())
  elseif face == 1 then
    print("I am facing "..getCardinality())
  elseif face == 2 then
    print("I am facing "..getCardinality())
  elseif face == 3 then
    print("I am facing "..getCardinality())
  end
end

function getCardinality()
  local card
  if face == 0 then
    card = "south"
  elseif face == 1 then
    card = "west"
  elseif face == 2 then
    card = "north"
  elseif face == 3 then
    card = "east"
  end
  return card
end

function rotate()
  turtle.turnRight()
  face = face + 1
  if face > 3 then
    face = 0
  end
end


math.randomseed( os.getComputerID() )
sleep(math.random(1,10))

turtle.select(16)
setLocationData()

while true do
  print("Checking "..getCardinality().." side...")
  
  if not turtle.detect() then
    print("sapling missing...")
    plantSappling()
  end
    
  if turtle.compare() then
    print("growth detected...")
    rednet.open("right")
    local hX = xPos
    local hZ = zPos
    
    if face == 0 then
      hZ = hZ+1
    elseif face == 1 then
      hX = hX-1
    elseif face == 2 then
      hZ = hZ-1
    elseif face == 3 then
      hX = hX+1
    end
    
    local counter = 1
    while turtle.compare() do
      print("asking for harvest..."..counter.." @ "..hX.." "..hZ.." on "..getCardinality().." side")
      rednet.send(ljID, "harvest "..hX.." "..hZ)
      print("message sent...")
      counter = counter + 1
      sleep(10)
    end
    rednet.close("right")
    print("harvested...")           
    plantSappling()
  else
    print("nothing to harvest...")
    sleep(15)
  end
  pickUpSapplings()
  rotate()
end