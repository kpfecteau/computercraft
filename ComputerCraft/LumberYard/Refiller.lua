local xPos, yPos, zPos
local baseX, baseY, baseZ

function setLocationData()
  xPos, yPos, zPos = gps.locate()
end

function refill()
  turtle.up()
  turtle.dropUp()
  turtle.down()
end

function moveForward()
  turtle.forward()
  setLocationData()
end
 
function goToTree(r,c)
  setLocationData()
  while xPos ~= r do
    moveForward()
  end
  turtle.turnLeft()
  while zPos ~= c do
    moveForward()
  end
end
 
function returnToStation()
  while zPos ~= baseZ do
    turtle.back()
    setLocationData()
  end
  turtle.turnRight()
  while xPos ~= baseX do
    turtle.back()
    setLocationData()
  end
  consolidate()
end

function consolidate()
  for i=1, 16 do
    local notEmpty = false
    for j=i, 16 do
      if turtle.getItemCount(j) > 0 then
        notEmpty = true
        turtle.select(j)
        turtle.transferTo(i)
      end
    end
    if not notEmpty then
      break
    end
  end
  turtle.select(1)
end
 
rednet.open("right")
setLocationData()
baseX = xPos
baseY = yPos
baseZ = zPos
while true do
  rednet.open("right")
  print("Waiting for message...")
  local sid, label, dis = rednet.receive()
  rednet.close("right")
  local row, col
  local info = {}
 
  for word in label:gmatch("%S+") do 
    table.insert(info, word) 
  end
  
  if info[1] == "refill" then
    print("Received refill message @ ")
    row = tonumber(info[2])
    col = tonumber(info[3])
    print("X: "..info[2].." Y:"..info[3])
    
    turtle.suckDown()
    goToTree(row,col)
    refill()
    returnToStation()
  end
  

end