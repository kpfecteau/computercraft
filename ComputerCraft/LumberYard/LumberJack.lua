local xPos, yPos, zPos
local baseX, baseY, baseZ

function setLocationData()
  xPos, yPos, zPos = gps.locate()
end

function chopTree()
  if turtle.compareDown() then
    turtle.digDown()
  end
  while turtle.compareUp() do
    turtle.digUp()
    turtle.up()
  end
  setLocationData()
  while yPos ~= baseY do
    turtle.digDown()
    turtle.down()
    setLocationData()
  end
end

function moveForward()
  if turtle.detect() then
    turtle.dig()
    turtle.forward()
    chopTree()
  else
    turtle.forward()
  end
  setLocationData()
end

function goToTree(r,c)
  setLocationData()
  while xPos ~= r do
    moveForward()
  end
  turtle.turnLeft()
  while zPos ~= c do
    moveForward()
  end
end
 
function returnToStation()
  turtle.turnLeft()
  turtle.turnLeft()
  while zPos ~= baseZ do
    moveForward()
  end
  turtle.turnLeft()
  while xPos ~= baseX do
    turtle.back()
    setLocationData()
  end
  sleep(1)
  rs.setOutput("back", true)
  sleep(6)
  rs.setOutput("back", false)
end
 
 
rednet.open("right")
setLocationData()
baseX = xPos
baseY = yPos
baseZ = zPos
while true do
  rednet.open("right")
  local sid, label, dis = rednet.receive()
  rednet.close("right")
  local row, col
  local info = {}
  
  for word in label:gmatch("%S+") do 
    table.insert(info, word) 
  end
  
  if info[1] == "harvest" then
    print("Received harvest message @ ")
    row = tonumber(info[2])
    col = tonumber(info[3])
    print("X: "..info[2].." Y:"..info[3])
    
    goToTree(row,col)
    chopTree()
    returnToStation()
  end
end