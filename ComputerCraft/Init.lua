-- http://pastebin.com/CcqKxdfJ
-- pastebin get CcqKxdfJ init

function downloadSetup()
  local content = http.get("https://bitbucket.org/kpfecteau/computercraft/raw/master/ComputerCraft/Setup/setup.lua").readAll()
  if not content then
    error("Could not download file...")
  end
  local f = fs.open("/bb/Setup/setup", "w")
  f.write(content)
  f.close()
  shell.run("/bb/Setup/setup")
end

local args = {...}

if not fs.exists("/bb/Setup") then
  fs.makeDir("/bb/Setup")
end

if args[1] == "update" then
  if args[2] == "self" then
    fs.delete("init")
    shell.run("pastebin", "get", "CcqKxdfJ", "init")
  else
    fs.delete("/bb/Setup/setup")
    downloadSetup()
    shell.run("/bb/Setup/setup")
  end
else
  downloadSetup()
end